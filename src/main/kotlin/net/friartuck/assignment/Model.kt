/*
 *
 *  * Copyright(c) FriarTuck Pte Ltd ("FriarTuck"). All Rights Reserved.
 *  *
 *  * This software is the confidential and proprietary information of FriarTuck.
 *  * ("Confidential Information").  You shall not disclose such Confidential
 *  * Information and shall use it only in accordance with the terms of the
 *  * license agreement you entered into with FriarTuck.
 *  *
 *  * FriarTuck MAKES NO REPRESENTATIONS OR WARRANTIES ABOUT THE SUITABILITY OF
 *  * THE SOFTWARE, EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED
 *  * TO THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 *  * PARTICULAR PURPOSE, OR NON-INFRINGEMENT. FriarTuck SHALL NOT BE LIABLE FOR
 *  * ANY DAMAGES SUFFERED BY LICENSEE AS A RESULT OF USING, MODIFYING OR
 *  * DISTRIBUTING THIS SOFTWARE OR ITS DERIVATIVES.
 *
 */

package net.friartuck.assignment
import ft.engine.accord.Engine
import ft.engine.accord.Lotus
import java.io.FileWriter
import java.util.*
import kotlin.concurrent.schedule


class Model : Observer {

    var newSolutionFound = false
    lateinit var engine: Engine
    lateinit var bestSoFar: Array<String>
    val BigScore: Long = 1000

    override fun update(o: Observable?, arg: Any?) {
        newSolutionFound = true
        bestSoFar = engine.nonZeroVariables
        parseSolution(bestSoFar)
        newSolutionFound = false
    }

    fun Solve() {
        engine = Lotus()
        engine.setSolutionObserver(this)
        // for Lotus engine setting
        engine.setParameter("RANDOMSOFT", 500)
        engine.setParameter("SOFTTABU", 50)
        engine.setParameter("BRANCHBACKLIMIT", 5)
        engine.setParameter("TRIES_BEFORE_RELAXATION", 10)
        engine.setParameter("FLIPSRATIO", 20)
        engine.setParameter("HARD_RESTART_RATE", 20) // 50
        engine.setParameter("PARTIALLIMIT", 50)
        engine.setParameter("HARDTABU", 10)
        engine.setParameter("HARD_RESTART_RATE", 20)

        val objFunction = engine.objective
        val numWeeks: Int = 4
        val numberOfDaysPerWeek: Int = 7
        val numTimeBlocks: Int = numWeeks * numberOfDaysPerWeek
        val numStaff: Int = 9
        val numberOfShifts: Int = 7
        val sumDurations: Int = 44
        //define staff group
        val staffList: MutableList<String> = ArrayList();
        for (x in 0..numStaff - 1) {
            staffList.add(x.toString())
        }
        val canFourHoursShiftStaff: List<String> = arrayListOf("0", "2", "5")
        val canNotFourHoursShiftStaff: List<String> = arrayListOf("1", "3", "4", "6", "7", "8")
        val staffAG1: List<String> = arrayListOf("0", "1", "2", "3")
        val staffAG2: List<String> = arrayListOf("6", "7", "8")
        val staffAG3: List<String> = arrayListOf("4", "5")
        val staffGroupAG = mutableMapOf<Int, List<String>>()
        staffGroupAG[1] = staffAG1
        staffGroupAG[2] = staffAG2
        staffGroupAG[3] = staffAG3
        val fixedShift: List<String> = arrayListOf("0", "5")
        val staffAlwaysOffOnPh: List<String> = arrayListOf("1", "2", "3", "8")
        //define all shift, morning shift, afternoon shift, off shift
        var allShiftList: List<String> = arrayListOf("M1", "M2", "M3", "A1", "A2", "DO", "PH")
        var morningShiftList: List<String> = arrayListOf("M1", "M2", "M3")
        var afternoonShiftList: List<String> = arrayListOf("A1", "A2")
        var offShift: List<String> = arrayListOf("DO", "PH")

        //define schedule period, working days, public holidays
        val schedulePeriod: MutableList<Int> = ArrayList();
        for (x in 0..numTimeBlocks - 1) {
            schedulePeriod.add(x)
        }
        val ph: List<Int> = arrayListOf(15, 16, 22, 23)
        val weekdays: MutableList<Int> = ArrayList()
        for (i in 0 until numWeeks) {
            for (j in 0..4) {
                weekdays.add(i * 7 + j)
            }
        }
        weekdays.removeAll(ph)
        //define working hours for eah shift
        val shiftDurations: List<Int> = arrayListOf(8, 7, 4, 8, 7, 0, 8)
        // Max 1 task assigned in 1 time-block per staff
        println("Constraint 1: Max 1 task assigned per staff per timeblock")
        for (n in staffList) {
            for (k in 0 until numTimeBlocks) {
                val atMostCons = engine.newConstraint(-1)
                engine.setConstraintPriority(atMostCons)
                val atLeastCons = engine.newConstraint(1)
                engine.setConstraintPriority(atLeastCons)

                for (t in allShiftList) {
                    // AT MOST 1 Shift per day!
                    engine.addConstraintTerm(atMostCons, -1, X(n, t, k))
                    // AT LEAST 1 Shift per day!
                    engine.addConstraintTerm(atLeastCons, 1, X(n, t, k))
                }
            }
        }

        val penaltyCoverage = 1;
        // Constraints, coverage, morning shift AG1/AG2/AG3 4(WD) Soft,   3(WE) Hard, Equal to
        // Constraints, coverage, afternoon shift AG1/AG2/AG3 3(WD) Hard,  3(WE) Soft, Equal to
        for (k in schedulePeriod) {
            val allMorningsCoverageWDAtLeast = engine.newSoftConstraint(4, -penaltyCoverage, true);
            val allMorningsCoverageWDAtMost = engine.newSoftConstraint(-4, -penaltyCoverage, true);
            val allMorningsCoverageWEAtLeast = engine.newConstraint(3);
            val allMorningsCoverageWEAtMost = engine.newConstraint(3);
            val allAfternoonsCoverageWDAtLeast = engine.newConstraint(3)
            val allAfternoonsCoverageWDAtMost = engine.newConstraint(-3)
            val allAfternoonsCoverageWEAtLeast = engine.newSoftConstraint(3, -penaltyCoverage, true)
            val allAfternoonsCoverageWEAtMost = engine.newSoftConstraint(-3, -penaltyCoverage, true)
            for (staff in staffList) {
                for (t in morningShiftList) {
                    when {
                        k in weekdays -> {
                            engine.addConstraintTerm(allMorningsCoverageWDAtLeast, 1, X(staff, t, k));
                            engine.addConstraintTerm(allMorningsCoverageWDAtMost, -1, X(staff, t, k));
                        }
                        else -> {
                            engine.addConstraintTerm(allMorningsCoverageWEAtLeast, 1, X(staff, t, k));
                            engine.addConstraintTerm(allMorningsCoverageWEAtMost, -1, X(staff, t, k));
                        }
                    }
                }
                for (t in afternoonShiftList) {
                    when {
                        k in weekdays -> {
                            engine.addConstraintTerm(allAfternoonsCoverageWDAtLeast, 1, X(staff, t, k));
                            engine.addConstraintTerm(allAfternoonsCoverageWDAtMost, -1, X(staff, t, k));
                        }
                        else -> {
                            engine.addConstraintTerm(allAfternoonsCoverageWEAtLeast, 1, X(staff, t, k));
                            engine.addConstraintTerm(allAfternoonsCoverageWEAtMost, -1, X(staff, t, k));
                        }
                    }
                }
            }
            engine.setConstraintPriority(allMorningsCoverageWEAtLeast)
            engine.setConstraintPriority(allMorningsCoverageWEAtMost)
            engine.setConstraintPriority(allAfternoonsCoverageWDAtLeast)
            engine.setConstraintPriority(allAfternoonsCoverageWDAtMost)
        }
//      Constraints, coverage, morning shift AG 1/2/3 (WD)  1 (WE) Soft, At least
//      Constraints, coverage, afternoon shift AG 1/2/3 (WD) 1 (WE) Hard, At least
        for (k in schedulePeriod) {
            for (key in staffGroupAG.keys) {
                val allAGMorningsCoverageWD = engine.newSoftConstraint(1, -penaltyCoverage, true);
                val allAGMorningsCoverageWE = engine.newSoftConstraint(1, -penaltyCoverage, true);
                val allAGAfternoonCoverageWD = engine.newConstraint(1);
                val allAGAfternoonCoverageWE = engine.newConstraint(1);
                for (staff in staffGroupAG[key]!!) {
                    for (t in morningShiftList) {
                        when {
                            k in weekdays -> engine.addConstraintTerm(
                                allAGMorningsCoverageWD,
                                1,
                                X(staff, t, k)
                            )
                            else -> engine.addConstraintTerm(allAGMorningsCoverageWE, 1, X(staff, t, k))
                        }
                    }
                    for (t in afternoonShiftList) {
                        when {
                            k in weekdays ->
                                engine.addConstraintTerm(allAGAfternoonCoverageWD, 1, X(staff, t, k))
                            else -> engine.addConstraintTerm(allAGAfternoonCoverageWE, 1, X(staff, t, k))
                        }
                    }
                }
                engine.setConstraintPriority(allAGAfternoonCoverageWD)
                engine.setConstraintPriority(allAGAfternoonCoverageWE)
            }
        }

        val requirmentPenalty: Int = 1
        // All staff must have only 1 day off, which is shift DO, per week
        for (staff in staffList) {
            for (j in 0 until numWeeks) {
                val oneDayOffAtMost = engine.newConstraint(-1);
                val oneDayOffAtLeast = engine.newConstraint(1);
                for (k in 0 until numberOfDaysPerWeek) {
                    if ((j * 7 + k) in ph)
                        continue
                    engine.addConstraintTerm(oneDayOffAtMost, -1, X(staff, "DO", j * 7 + k))
                    engine.addConstraintTerm(oneDayOffAtLeast, 1, X(staff, "DO", j * 7 + k))
                }
                engine.setConstraintPriority(oneDayOffAtMost)
                engine.setConstraintPriority(oneDayOffAtLeast)
            }
        }
        //        All staff must have exactly 44 hours working hours per week
        for (staff in staffList) {
            for (j in 0 until numWeeks) {
                val fourtyFourHoursAtLeast = engine.newConstraint(sumDurations);
                val fourtyFourHoursAtMost = engine.newConstraint(-sumDurations);
                for (k in 0 until numberOfDaysPerWeek) {
                    for (i in 0 until numberOfShifts) {
                        engine.addConstraintTerm(
                            fourtyFourHoursAtLeast,
                            shiftDurations[i],
                            X(staff, allShiftList[i], j * 7 + k)
                        );
                        engine.addConstraintTerm(
                            fourtyFourHoursAtMost,
                            -shiftDurations[i],
                            X(staff, allShiftList[i], j * 7 + k)
                        );
                    }
                }
                engine.setConstraintPriority(fourtyFourHoursAtLeast);
                engine.setConstraintPriority(fourtyFourHoursAtMost);
            }
        }
        //      Achieve Always Off on Public Holidays. If a staff is off on Public Holiday, the shift has to be PH
        for (staff in staffAlwaysOffOnPh) {
            for (j in ph) {
                val alwaysOffOnPH = engine.newConstraint(1);
                engine.addConstraintTerm(
                    alwaysOffOnPH,
                    1,
                    X(staff, "PH", j)
                );
                engine.setConstraintPriority(alwaysOffOnPH);
            }
        }
        for (staff in staffList) {
            for (j in 0 until numTimeBlocks) {
                if (j in ph)
                    continue
                val neveNoOnPH = engine.newConstraint(0);
                engine.addConstraintTerm(
                    neveNoOnPH,
                    -1,
                    X(staff, "PH", j)
                );
                engine.setConstraintPriority(neveNoOnPH);
            }
        }
//     Achieve 0.5 working days per week for staffs who need it
        for (staff in canFourHoursShiftStaff) {
            for (j in 0 until numWeeks) {
                val halfWorkingDayAtLeast = engine.newConstraint(1);
                val halfWorkingDayAtMost = engine.newConstraint(-1);
                for (k in 0 until numberOfDaysPerWeek) {
                    engine.addConstraintTerm(halfWorkingDayAtLeast, 1, X(staff, "M3", j * 7 + k));
                    engine.addConstraintTerm(halfWorkingDayAtMost, -1, X(staff, "M3", j * 7 + k));
                }
                engine.setConstraintPriority(halfWorkingDayAtLeast)
                engine.setConstraintPriority(halfWorkingDayAtMost)
            }
        }
//      Undesired 0.5 working days per week for staffs who do NOT need it
        for (staff in canNotFourHoursShiftStaff) {
            val halfWorkingDayNoNeed = engine.newConstraint(0)
            for (j in 0 until numWeeks) {
                for (k in 0 until numberOfDaysPerWeek)
                    engine.addConstraintTerm(halfWorkingDayNoNeed, -1, X(staff, "M3", j * 7 + k))
            }
            engine.setConstraintPriority(halfWorkingDayNoNeed)
        }
        val patternPenalty: Int = 2;
//       Undesired Afternoon Shift after an off day (off day can be DO or PH), pattern
        for (staff in staffList) {
            for (j in 0..numTimeBlocks - 1) {
                val afternoonAfterOffDay = engine.newSoftConstraint(-1, -patternPenalty, true);
                for (t in offShift)
                    engine.addConstraintTerm(
                        afternoonAfterOffDay,
                        -1,
                        X(staff, t, j)
                    )
                for (t in afternoonShiftList)
                    engine.addConstraintTerm(
                        afternoonAfterOffDay,
                        -1,
                        X(staff, t, j + 1)
                    )
            }
        }
////        Undesired 3 consecutive Afternoon Shifts, pattern
        for (staff in staffList) {
            for (j in 0..numTimeBlocks - 3) {
                val threeConsecutive = engine.newSoftConstraint(-2, -patternPenalty, true);
                for (t in afternoonShiftList) {
                    engine.addConstraintTerm(
                        threeConsecutive,
                        -1,
                        X(staff, t, j)
                    )
                    engine.addConstraintTerm(
                        threeConsecutive,
                        -1,
                        X(staff, t, j + 1)
                    )
                    engine.addConstraintTerm(
                        threeConsecutive,
                        -1,
                        X(staff, t, j + 2)
                    )
                }
            }
        }
//        Undesired mixed AM-PM for “Fixed Shift Group”s, pattern
        for (staff in fixedShift) {
            for (j in 0..numTimeBlocks - 1) {
                val fixAMPM = engine.newSoftConstraint(-1, -patternPenalty, true);
                val fixPMAM = engine.newSoftConstraint(-1, -requirmentPenalty, true);
                for (t in morningShiftList) {
                    engine.addConstraintTerm(
                        fixAMPM,
                        -1,
                        X(staff, t, j)
                    )
                    engine.addConstraintTerm(
                        fixPMAM,
                        -1,
                        X(staff, t, j + 1)
                    )
                }
                for (t in afternoonShiftList) {
                    engine.addConstraintTerm(
                        fixAMPM,
                        -1,
                        X(staff, t, j + 1)
                    )
                    engine.addConstraintTerm(
                        fixPMAM,
                        -1,
                        X(staff, t, j)
                    )
                }
            }
        }

        engine.clearAllInitialization()
        engine.preprocess()

        Timer("SettingUp", false).schedule(60 * 1000) {
            engine.setFinish()
            if (bestSoFar.isNotEmpty()) {
                var fileWriter: FileWriter? = null
                fileWriter = FileWriter("solution.csv")
                fileWriter.append(',')
                for (d in 1..numTimeBlocks - 1) {
                    fileWriter.append(d.toString() + ",")
                }
                fileWriter.append(numTimeBlocks.toString() + '\n')
                for (n in staffList) {
                    val u = n.toInt()+1
                    fileWriter.append("Staff" + u.toString() + ",")
                    for (d in 0..numTimeBlocks - 1)
                        for (s in allShiftList) {
                            if (bestSoFar.contains("X_=${n}=${s}=${d}")) {
                                fileWriter.append(s.toString() + ",")
                            }
                        }
                    fileWriter.append('\n')
                }
                fileWriter.close()
            }
            this.cancel()
        }
        engine.mainLoop()
    }

    fun X(staff: String, task: String, time: Int): String {
        return "X_=${staff}=${task}=${time}"
    }

    fun parseSolution(solution: Array<String>) {
        if (solution.isNotEmpty()) {
            solution.forEach {
                if (it.contains("X=")) {
                    val tokens = it.split(delimiters = *arrayOf("="))
                    println("Staff ${tokens[1]} is assigned Shift ${tokens[2]} on Day ${tokens[3]}")
                }
            }
        }
    }
}


fun main(args: Array<String>) {
    var assignment: Model = Model()
    assignment.Solve()
}


